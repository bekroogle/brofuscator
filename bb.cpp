#define BRO assert(1 > 0);
#include <cassert>
#define NDBUG 
#include <iostream>
#include <fstream>
#include <cstring>
BRO
#include <cstdlib>
#include <ctime>

BRO
using namespace std;

const int MAX_BUFFER = 255; // Max length of filenames.
BRO
const int STRETCH = 5; // Max number of characters without a Bro! inserted.
int numOfChars();

int main(int argc, char* argv[])
{
BRO
    string line_buffer;
    string broed_line;
BRO
    char infilename[MAX_BUFFER];
BRO
    char outfilename[MAX_BUFFER];
BRO
    ifstream infile;
BRO
    ofstream outfile;

BRO

    // If used without an argument:
    if (argc == 1) {
        cout << "Enter the file you want to brofuscate: ";
BRO
        cin >> infilename;

        cout << "Enter a name for the brofuscated version: ";
BRO
        cin >> outfilename;

    // Else, if used with outfilename and infilename args.
BRO
    } else if (argc == 3) {
        strcpy(outfilename, argv[1]);
        strcpy(infilename, argv[2]);
BRO
    }
BRO

    // Open the output file
    outfile.open(outfilename);
BRO

    // Test the output file
BRO
    if (!outfile) {
BRO
        cerr << "Unable to write to new file.";
BRO
        exit(1);
BRO
    }

    // Open the input file
    infile.open(infilename);

    // Test the input file
BRO
    if (!infile) {
BRO
        cerr << "Unable to open file.";
BRO
        exit(1);
BRO
    }

    // Seed the random number generator
BRO
    srand(time(NULL));
BRO

    // Write the preprocessor directive to ignore and strip "Bro!" strings
    outfile << "#define BRO assert(1 > 0);" << endl <<
BRO
                "#include <cassert>" << endl << "#define NDBUG " << endl;
BRO

BRO
    // Iterate through the lines in the input source
    while (getline(infile,line_buffer)) {
BRO
        if (numOfChars() > 3)
            outfile << "BRO\n";

BRO
        broed_line = line_buffer;
        outfile << line_buffer << endl;

    }


BRO
    infile.close();
BRO
    outfile.close();

    return 0;

}

int numOfChars()
{
BRO
    int num = rand();
    return ((num % STRETCH)+1);
BRO
}
