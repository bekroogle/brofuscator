#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdlib>
#include <ctime>

using namespace std;

const int MAX_BUFFER = 255; // Max length of filenames.
const int STRETCH = 5; // Max number of characters without a Bro! inserted.
int numOfChars();

int main(int argc, char* argv[])
{
    string line_buffer;
    string broed_line;
    char infilename[MAX_BUFFER];
    char outfilename[MAX_BUFFER];
    ifstream infile;
    ofstream outfile;


    // If used without an argument:
    if (argc == 1) {
        cout << "Enter the file you want to brofuscate: ";
        cin >> infilename;

        cout << "Enter a name for the brofuscated version: ";
        cin >> outfilename;

    // Else, if used with outfilename and infilename args.
    } else if (argc == 3) {
        strcpy(outfilename, argv[1]);
        strcpy(infilename, argv[2]);
    }

    // Open the output file
    outfile.open(outfilename);

    // Test the output file
    if (!outfile) {
        cerr << "Unable to write to new file.";
        exit(1);
    }

    // Open the input file
    infile.open(infilename);

    // Test the input file
    if (!infile) {
        cerr << "Unable to open file.";
        exit(1);
    }

    // Seed the random number generator
    srand(time(NULL));

    // Write the preprocessor directive to ignore and strip "Bro!" strings
    outfile << "#define BRO assert(1 > 0);" << endl <<
                "#include <cassert>" << endl << "#define NDBUG " << endl;

    // Iterate through the lines in the input source
    while (getline(infile,line_buffer)) {
        if (numOfChars() > 3)
            outfile << "BRO\n";

        broed_line = line_buffer;
        outfile << line_buffer << endl;

    }


    infile.close();
    outfile.close();

    return 0;

}

int numOfChars()
{
    int num = rand();
    return ((num % STRETCH)+1);
}