Brofuscator
===========
-This program is designed to read in a C/C++ source file, prepend it with
 a preprocessor directive that strips out the character string "Bro!"
 any place it is found in the source.

-It then, pseudorandomly splices the "Bro!" string into the source code.
 This has no effect on the operation of the program, rather, it obfuscates
 the source code by means of "Brofuscation."
