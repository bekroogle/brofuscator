#DEFINE Bro!  
Bro!#include <iostream>
#include <fstream>
Bro!#include <cstring>
Bro!#include <cstdlib>
Bro!#include <ctime>

using namespace std;

const int MAX_BUFFER = 255; // Max length of filenames.
const int STRETCH = 5; // Max number of characters without a Bro! inserted.
int numOfChars();

int main(int argc, char* argv[])
{
    string line_buffer;
    string broed_line;
Bro!    char infilename[MAX_BUFFER];
    char outfilename[MAX_BUFFER];
Bro!    ifstream infile;
Bro!    ofstream outfile;

Bro!
    // If used without an argument:
Bro!    if (argc == 1) {
        cout << "Enter the file you want to brofuscate: ";
Bro!        cin >> infilename;
Bro!
        cout << "Enter a name for the brofuscated version: ";
        cin >> outfilename;
Bro!
    // Else, if used with outfilename and infilename args.
    } else if (argc == 3) {
Bro!        strcpy(outfilename, argv[1]);
Bro!        strcpy(infilename, argv[2]);
    }

Bro!    // Open the output file
Bro!    outfile.open(outfilename);
Bro!
Bro!    // Test the output file
    if (!outfile) {
        cerr << "Unable to write to new file.";
        exit(1);
Bro!    }
Bro!
Bro!    // Open the input file
Bro!    infile.open(infilename);

Bro!    // Test the input file
Bro!    if (!infile) {
        cerr << "Unable to open file.";
Bro!        exit(1);
Bro!    }
Bro!
    // Seed the random number generator
    srand(time(NULL));
Bro!
    // Write the preprocessor directive to ignore and strip "Bro!" strings
Bro!    outfile << "#DEFINE Bro!  \n";
Bro!
    // Iterate through the lines in the input source
Bro!    while (getline(infile,line_buffer)) {
Bro!        if (numOfChars() > 3)
            outfile << "Bro!";

        broed_line = line_buffer;
        outfile << line_buffer << endl;
Bro!
    }

Bro!
    infile.close();
Bro!    outfile.close();

    return 0;

Bro!}
Bro!
int numOfChars()
Bro!{
    int num = rand();
    return ((num % STRETCH)+1);
Bro!}
